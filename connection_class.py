"""Connect to BigQuery"""
from google.cloud import bigquery
from google.oauth2 import service_account

class Connection:
    def python_to_bigquery_conn(self):
        test_attribute = "THIS IS A TEST ATTRIBUTE"
        credentials = service_account.Credentials.from_service_account_file('bigquery-test-project-365508-7e857141d1e2.json')
        project_id = 'bigquery-test-project-365508'
        client = bigquery.Client(credentials= credentials,project=project_id)
        return client

obj = Connection()
client = obj.python_to_bigquery_conn()

# query_job = client.query("""
#    SELECT *
#    FROM `bigquery-test-project-365508.san_francisco_bikeshare.bikeshare_trips`
#    LIMIT 5 """)

# results = query_job.result() # Wait for the job to complete.
# # print(format(results))

# print("The data:")
# for row in results:
#     print("name={}".format( row))
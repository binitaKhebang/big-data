import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
// import "@/plugins/echarts"; // import echarts
import ECharts from 'vue-echarts'
import { use } from "echarts/core"
import PrimeVue from 'primevue/config'; // import Prime vue for data visualization
import Chart from 'primevue/chart';

import {
    CanvasRenderer
} from 'echarts/renderers'

import {
    BarChart
} from 'echarts/charts'

import {
    GridComponent,
    TooltipComponent
} from 'echarts/components'


use([
    CanvasRenderer,
    BarChart,
    GridComponent,
    TooltipComponent
])

const app = createApp(App)

app.component('v-chart', ECharts)
app.component('p-chart', Chart)

app.use(router)
app.use(PrimeVue)

app.mount('#app')

// createApp(App).use(router).mount('#app')
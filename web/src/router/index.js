
import { createWebHistory, createRouter } from 'vue-router';
// import Home from '@/views/Home.vue';
import DataReport from '@/views/DataReport.vue'
import BarDataReport from '@/views/BarDataReport.vue'
import PrimeVueBarGraph from '@/views/PrimeVueBarGraph.vue'

const routes = [
	{
		path: '/data-report',
		name: 'DataReport',
		component: DataReport
	},
	{
		path: '/bar-data-report',
		name: 'BarDataReport',
		component: BarDataReport
	},
	{
		path: '/prime-vue-data-report',
		name: 'PrimeVueBarGraph',
		component: PrimeVueBarGraph
	}
];

const router = createRouter({
	history: createWebHistory(),
	routes,
});

export default router;
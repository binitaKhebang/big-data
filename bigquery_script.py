# from connection_class import Connection
import connection_class
import pandas as pd
import db_dtypes
import gspread
# from google.oauth2 import service_account
from oauth2client.service_account import ServiceAccountCredentials
import gspread_dataframe as gd
from gspread_dataframe import get_as_dataframe, set_with_dataframe
import json

client = connection_class.Connection().python_to_bigquery_conn()

query_job = client.query("""
    CREATE OR REPLACE PROCEDURE
    `bigquery-test-project-365508.san_francisco_bikeshare.bike_trips_count_sp`()
    BEGIN
    SELECT
    DISTINCT bst.start_station_name AS start_station,
    COUNT(DISTINCT trip_id) AS trips_count
    FROM
    `bigquery-test-project-365508.san_francisco_bikeshare.bikeshare_trips` bst
    LEFT JOIN
    `bigquery-test-project-365508.san_francisco_bikeshare.bikeshare_station_info` bsi
    ON
    CAST(bst.start_station_id AS string) = bsi.station_id
    AND CAST(bst.end_station_id AS string) = bsi.station_id
    LEFT JOIN
    `bigquery-test-project-365508.san_francisco_bikeshare.bikeshare_regions` bsr
    ON
    bsi.region_id = bsr.region_id
    WHERE
    bst.start_station_name IS NOT NULL
    GROUP BY
    start_station
    ORDER BY
    start_station;
    END""")

call_sp = client.query("""
    CALL
        `bigquery-test-project-365508.san_francisco_bikeshare.bike_trips_count_sp`()
    """)

results = call_sp.result()
results = [tuple(row) for row in results]

# Authenticate 
gc = gspread.service_account(filename="bigquery-test-project-365508-7e857141d1e2.json")

# Connect to Google Sheets
scope = ['https://www.googleapis.com/auth/spreadsheets',
         "https://www.googleapis.com/auth/drive"]

credentials = ServiceAccountCredentials.from_json_keyfile_name("bigquery-test-project-365508-7e857141d1e2.json", scope)
client = gspread.authorize(credentials)

sheet = client.create("TestDatabase")
sheet.share("binitakhebang@gmail.com", perm_type='user', role='writer')

# Write to dataframe
df = pd.DataFrame(results)
print(df)
print('-----------')
print(df)

worksheet = sheet.add_worksheet("Bigquery Data", 400, 5)
worksheet = sheet.worksheet('Bigquery Data')

worksheet.insert_rows(df.values.tolist())
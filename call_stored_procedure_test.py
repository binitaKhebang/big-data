# from connection_class import Connection
import connection_class

client = connection_class.Connection().python_to_bigquery_conn()

call_sp = client.query("""
    CALL    
        `bigquery-test-project-365508.san_francisco_bikeshare.bike_trips_count_sp`()
    """)

results = call_sp.result()

print("stored procedure called from another page")
for row in results:
    print(format(row))